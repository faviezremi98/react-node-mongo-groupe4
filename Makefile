start:
	docker-compose up -d
	docker-compose run server


install:
	docker-compose down
	cd server && npm install
	cd client && npm install

stop:
	docker-compose down
	docker container prune