const JWTVerifyToken = require("../lib/jwt-auth").verifyToken;
const { Marchand } = require("../models/sequelize");

const verifyToken = (req, res, next) => {
  const authHeader = req.get("Authorization");
  if (!authHeader) { //si y'a pas de Header on renvoit Unauthorized
    return res.sendStatus(401);
  } else if (authHeader.startsWith("Basic ")) { //On check si y'a un token Basic (pour le backoffice via ApiKey + ApiSecret)
    let keys = authHeader.replace("Basic ", "").split(':'); // le token ressemble a ça : Authorization Basic APIKEY:APISECRET

    authApiToken = keys[0];
    authApiSecret = keys[1];

    Marchand.findOne({
      apiToken: authApiToken, //On cherche notre marchand via ses credentials (apikey + secret)
      apiSecret: authApiSecret
    }).then((data) => {
      if (!data) {
        return res.sendStatus(401); //si pas de data, Unauthorized
      } else {
        req.user = data; //sinon on continue et on rajoute nos data user à notre requête
        next();
      }
    });
  } else if(authHeader.startsWith("Bearer ")) { //Si y'a pas de token Basic on check si y'a un token Bearer
    const token = authHeader.replace("Bearer ", ""); //Utilisé pour le site marchand client
    JWTVerifyToken(token)
      .then((payload) => {
        req.user = payload; //idem on rajoute nos data user à notre requête
        next();
      })
      .catch(() => res.status(500).send({ error: "test" }));
  }
};

module.exports = verifyToken;
