 const express = require("express");
const RouterManager = require("./routes");
const cors = require("cors");
const bodyparser = require('body-parser');
const TransactionMongo = require ("./models/Transaction");
const app = express();

app.use(cors());
app.use(express.json({limit: "50mb"}));


app.get("/hello", (req, res) => {
  res.json({ msg: "Hello" });
});

RouterManager(app);
app.use(bodyparser.json());

TransactionMongo.aggregate(
  [
    {
      $group:
        {
          _id: "$status",
          count: { $sum: 1 }
        }
    }
  ]
).then(result => console.log(("resutat",result)));


app.listen(3000, () => console.log("listening..."));