const Scrapper = require('./lib/Scrapper');
const Devise = require('./models/Devise.js');

let returnData = null;

const processData = ($) => {
  const data = [];
  $('.c-table tbody tr').each((index, tr)=> {
    var today = new Date();
    var date = today.getDate()+'/'+(today.getMonth()+1)+'/'+today.getFullYear();
    const fields = $(tr).find('td');
    data.push({
      devise: fields.eq(0).text().trim(),
      taux: Number(fields.eq(1).text().trim()),
      date: date
    })
  });
  const finalData = data.slice(0,5);
  returnData = finalData;
  return finalData;
}

const saveResult = finalData => {
  Promise.all(finalData.map(datum => {
    const devise = new Devise(datum);
    return devise.save();
  })).then(() => console.log('data saved'));
}

const launchScrapp = () => {
  return new Promise((resolve, reject) => {
    const req = Scrapper('https://www.boursorama.com/bourse/devises/taux-de-change/', {}, processData, saveResult)
    req.end();
    resolve(returnData);
  })
  }


launchScrapp();

module.exports = launchScrapp;
