const express = require("express");
const { Marchand } = require("../models/sequelize");
const router = express.Router();
const { Op, ValidationError } = require("sequelize");
const createToken = require("../lib/jwt-auth").createToken;

router.get('/marchands-by-admin/:id', (req, res) => {
    Marchand.findByPk(req.params.id)
        .then(async (data) => {
            let JWT = await createToken({ email: data.email }).then((token) => token);
            res.json({data, JWT})
        }
        )
})

module.exports = router;
