const express = require("express");
const { Marchand } = require("../models/sequelize");
const router = express.Router();
const { Op, ValidationError } = require("sequelize");
const createToken = require("../lib/jwt-auth").createToken;

const prettifyValidationErrors = (errors) =>
  errors.reduce((acc, field) => {
    acc[field.path] = [...(acc[field.path] || []), field.message];
    return acc;
  }, {});

router.get("/", (req, res) => {
  Marchand.findAll({
  })
    .then((data) => res.json(data))
    .catch((err) => res.sendStatus(500));
});

router.get('/email/:email', (req, res) => {
  Marchand.findOne({
      where: { email: req.params.email }
  })
  .then(data => res.json(data))
})

router.get('/activation', (req, res) => {
  Marchand.findAll({
    where: {
      [Op.and]: [
        {
          apiSecret: {
            [Op.is]: null
          }
        },
        {
          apiToken: {
            [Op.is]: null
          }
        }
      ]
    }
  }).then((data) => res.json(data))
});

router.put('/activate/:id', async(req, res) => {
  var user = req.body;

  var apiSecret = await createToken({id : user.id, email: user.email, denomSoc: user.denomSoc}).then((token) => token);
  var apiToken = await createToken({email: user.email, denomSoc: user.denomSoc}).then((token) => token);

  user.apiSecret = apiSecret;
  user.apiToken = apiToken;

  Marchand.update(user, {
    where: {
      id: req.params.id
    }, returning: true
  })
    .then(([nbUpdated, result]) =>
      nbUpdated ? res.json(result[0]) : res.sendStatus(404)
    )
  .catch(() => res.sendStatus(500));
});

router.put('/apiKey/:id', async(req, res) => {
  var user = req.body;
  var nbRdm = Math.random() * (100 - 2) + 2;
  var apiSecret = await createToken({id : user.id, email: user.email, denomSoc: user.denomSoc, nbRdm}).then((token) => token);
  var apiToken = await createToken({email: user.email, denomSoc: user.denomSoc, nbRdm}).then((token) => token);

  user.apiSecret = apiSecret;
  user.apiToken = apiToken;

  Marchand.update(user, {
    where: {
      id: req.params.id
    }, returning: true
  })
    .then(([nbUpdated, result]) =>
      nbUpdated ? res.json(result[0]) : res.sendStatus(404)
    )
  .catch(() => res.sendStatus(500));
});


/** Item Routes  **/
router.get("/:id", (req, res) => {
  Marchand.findByPk(req.params.id)
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
    .catch(() => res.sendStatus(500));
});



// PUT
router.put("/:id", (req, res) => {
  res.json({body: req.body});
  var user = req.body;
  Marchand.update(req.body, { where: { id: req.params.id }, returning: true })
    .then(([nbUpdated, result]) =>
      nbUpdated ? res.json(result[0]) : res.sendStatus(404)
    )
    .catch((err) => {
      if (err.name === "ValidationError") {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });
});

router.delete("/:id", (req, res) => {
  Marchand.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((nbDelete) => (nbDelete ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => res.sendStatus(500));
});

module.exports = router;
