const Router = require('express').Router;
const Transaction = require('../../models/Transaction');

const router = Router();

router.get('/', (req, res) => {
  Transaction.find().then(data => res.json(data));
});

router.get('/:id', (req, res) => {
  Transaction.findOne({Title: req.params.id}).then(data => {
    if(data) res.json(data);
    else res.sendStatus(404);
  }
  );
});

router.post('/', (req, res) => {
  const transaction = new Transaction(req.body);
  transaction.save(req.body)
    .then(data => res.status(201).json(data))
    .catch(error => {
      if(error.name === "ValidationError") {
        res.status(400).json(error.errors);
      } else {
        res.sendStatus(500);
      }
    });
});

router.delete('/:id', (req, res) => {
  Transaction.deleteOne({id: req.params.id}).then(() => res.sendStatus(204));
});

router.put('/:id', (req, res)=> {
  Transaction.findOneAndUpdate({id: req.params.id}, req.body).then(data=> { 
    res.json({...data, ...req.body})
  });
});

module.exports = router;
