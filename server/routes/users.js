const express = require("express");
const { User } = require("../models/sequelize");
const router = express.Router();
const { Op, ValidationError } = require("sequelize");

const prettifyValidationErrors = (errors) =>
  errors.reduce((acc, field) => {
    acc[field.path] = [...(acc[field.path] || []), field.message];
    return acc;
  }, {});

router.get("/", (req, res) => {
  User.findAll({
  })
    .then((data) => res.json(data))
    .catch((err) => res.sendStatus(500));
});

/** Item Routes  **/
router.get("/:id", (req, res) => {
  User.findByPk(req.params.id)
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
    .catch(() => res.sendStatus(500));
});

// PUT
router.put("/:id", (req, res) => {
  User.update(req.body, { where: { id: req.params.id }, returning: true })
    .then(([nbUpdated, result]) =>
      nbUpdated ? res.json(result[0]) : res.sendStatus(404)
    )
    .catch((err) => {
      if (err.name === "ValidationError") {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });
});

router.delete("/:id", (req, res) => {
  User.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((nbDelete) => (nbDelete ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => res.sendStatus(500));
});

module.exports = router;
