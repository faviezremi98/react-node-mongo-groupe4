const express = require("express");
const Devise = require("../models/Devise");
const router = express.Router();
const launchScrapp = require("../scrapping");
const { Marchand } = require("../models/sequelize");

router.get("/", async(req, res) => {
  const devises = await launchScrapp();
  
  Marchand.findOne({
    where:
    {
      email: req.user.email
    }
  }).then((data) => {
    if(!data)
    {
      res.status(404).send({msg: "Marchand Not Connected"})
    }

    let deviseMarchand = null;
    devises.forEach((element) => {
      if(data.currency !== "EUR" && element.devise === data.currency)
      {
        deviseMarchand = element;
      }
    });

    res.json({devise: deviseMarchand})

  })
  .catch((err) => {
    res.status(500).send({err})
  })
});


module.exports = router;
    
