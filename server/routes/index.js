const SecurityRouter = require("./security");
const UserRouter = require("./users");
const TransactionRouter = require("./transactions");
const MarchandRouter = require("./marchands");
const DeviseRouter = require("./devises");
const PSPRouter = require("./psp");
const TransactionMangoRouter = require("./mongo/transactionMongo");
const AdminRouter = require('./admins');
const MailRouter = require('./mail');
const HistoRouter = require('./histoTransaction');
const verifyToken = require("../middlewares/verifyToken");

const routerManager = (app) => {
  app.use("/payment" , PSPRouter) // Mon router PSP est là 
  app.use("/", SecurityRouter);
  app.use('/mail', MailRouter);
  app.use(verifyToken)
  app.use("/devises", DeviseRouter);
  app.use("/histo", HistoRouter);
  app.use("/transactions", TransactionRouter);
  app.use("/marchands", MarchandRouter);
  app.use("/users", UserRouter);
  app.use('/transactionMongo', TransactionMangoRouter);
  app.use('/admins', AdminRouter);
};

module.exports = routerManager;
