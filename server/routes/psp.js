const express = require("express");
const router = express.Router();
const { Transaction, Marchand, HistoTransaction } = require("../models/sequelize");

router.post('/histo/loading', (req, res) => {
    const transaction = req.body;
    transaction.status = "En cours";
    // res.json({total: transaction.total, status: transaction.status, MarchandId: transaction.MarchandId});
    HistoTransaction.create({ total: transaction.total, panier: transaction.panier, status: transaction.status, MarchandId: transaction.MarchandId }).then((data) => {
        res.json(data);
    }).catch((err) => res.status(500).send({ err }));
});

router.post('/histo/confirm', (req, res) => {
    const transaction = req.body;
    transaction.status = "confirmed";
    HistoTransaction.create({ id: transaction.id, total: transaction.total, panier: transaction.panier, status: transaction.status, MarchandId: transaction.MarchandId }).then((data) => {
        res.json(data);
    }).catch((err) => res.status(500).send({ err }))
})

router.post('/', (req, res) => {
    let transaction = req.body
    transaction.status = "En cours";
    Transaction.update(transaction, {
        where: {
            id: transaction.id
        },
        returning: true
    }).then(([nbUpdated, result]) =>
        Marchand.findByPk(transaction.MarchandId).then(data => {
            if (!data) {
                res.sendStatus(500)
            }
            nbUpdated ? res.json({ transaction: result[0], marchand: data }) : res.sendStatus(404)
        })
    )
        .catch(() => res.sendStatus(500));
})

router.post('/change', (req, res) => {
    const transaction = req.body;
    // HistoTransaction.create(transaction).then((data) => {
    //     res.json(data);
    // });
    transaction.status = "confirmed"
    Transaction.update(transaction, {
        where: {
            id: transaction.id
        },
        returning: true
    }).then(([nbUpdated, result]) => {
        Marchand.findByPk(transaction.MarchandId).then(data => {
            if (!data) {
                res.sendStatus(500)
            }
            nbUpdated ? res.json({ transaction: result[0], marchand: data }) : res.sendStatus(404)
        }

        )
            .catch(() => res.sendStatus(500));
    })
});

router.post('/remboursement', (req, res) => {
    const transaction = req.body;
    transaction.status = "refund";
    Transaction.update(transaction, {
        where: {
            id: transaction.id
        }, returning: true
    }).then(([nbUpdated, result]) => {
        nbUpdated ? res.json({ transaction: result[0] }) : res.sendStatus(404)
    })
});


module.exports = router;

//Ici je post ma transaction, et je lui change son statut