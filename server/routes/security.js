const { Router } = require("express");
const createToken = require("../lib/jwt-auth").createToken;
const { User, Marchand, Admin } = require("../models/sequelize");
const bcrypt = require("bcryptjs");
const router = Router();

router.post("/login_check", (req, res) => {
  const {email, password, marchand} = req.body;
  let resp;
  if(marchand)
  {
    resp = Marchand.findOne({
      where: { 'email': email },
    })
  } else {
    resp = User.findOne({
      where: { 'email': email },
    })
  }

    resp.then((data) => {
      if (!data) {  
        Admin.findOne({
          where: { 'email': email},
        }).then((data) => {
          if(!data)
          {
            res.status(401).json({
              email: "Invalid credentials",
              password: "Invalid credentials",
            });
          } else {
            bcrypt.compare(password, data.password).then((result) => {
              if (result) {
                createToken({ email })
                  .then((token) => res.json({ data, token, admin: true }))
                  .catch((error) => res.status(500).json({
                      'error': "test2"
                  }));
              } else {
                res.status(401).json({
                  email: "Invalid credentials",
                  password: "Invalid credentials",
                });
              }
            });
          }
        })
        
      } else {
        bcrypt.compare(password, data.password).then((result) => {
          if (result) {
            createToken({ email })
              .then((token) => res.json({ data, token }))
              .catch((error) => res.status(500).json({
                  'error': "test2"
              }));
          } else {
            res.status(401).json({
              email: "Invalid credentials",
              password: "Invalid credentials",
            });
          }
        });
      }
      
    })
    .catch((error) => res.status(500).json({'error': error}));
});



// POST - Create User (client - site marchand)
router.post("/users", (req, res) => {
  User.create(req.body)
    .then((data) => res.status(201).json(data))
    .catch((err) => {
      if (err instanceof ValidationError) {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });
});

//POST - Create Marchand (backoffice)
router.post("/marchands", (req, res) => {
  Marchand.create(req.body)
    .then((data) => res.status(201).json(data))
    .catch((err) => {
      if (err instanceof ValidationError) {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });
});

//POST - Create Admin (backoffice)
router.post("/admins", (req, res) => {
  Admin.create(req.body)
    .then((data) => res.status(201).json(data))
    .catch((err) => {
      if (err instanceof ValidationError) {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });
});
module.exports = router;

