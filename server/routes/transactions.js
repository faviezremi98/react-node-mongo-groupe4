const express = require("express");
const { Transaction } = require("../models/sequelize");
const TransactionMongo = require ("../models/Transaction");
const router = express.Router();
const { Op, ValidationError } = require("sequelize");

const prettifyValidationErrors = (errors) =>
  errors.reduce((acc, field) => {
    acc[field.path] = [...(acc[field.path] || []), field.message];
    return acc;
  }, {});

router.get("/status", (req, res) => {
  
  TransactionMongo.aggregate(
    [{$group:{_id: "$status",count: {$sum: 1}}}]).then((data) => (data ? res.json(data) : res.sendStatus(404)))
  .catch((err) => console.log(err) || res.sendStatus(500));
  });

router.get("/count", (req, res) => {
  TransactionMongo.find().count().then(data => res.json(data));
});
router.get("/", (req, res) => {
/*   TransactionMongo.find().then(data => res.json(data));
 */
   Transaction.findAll({
  }) 
    .then((data) => res.json(data))
    .catch((err) => res.sendStatus(500));
});


//POST 
router.post("/", (req, res) => {
  const transaction = new TransactionMongo(req.body);
  transaction.save(req.body);
  var obj = { total: req.body.total, panier : req.body.panier, status: "created", MarchandId: req.user.id }
  Transaction.create(obj)
    .then((data) => {
      res.status(201).json({data, url: "http://localhost:3002/payment"})
    }
    )
    .catch((err) => {
      if (err instanceof ValidationError) {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });
}); 

/* 
router.post("/", (req, res) => {
  const transaction = new TransactionMongo(req.body);
  transaction.save(req.body);

  Transaction.create(req.body)
  .then((data) => {
    res.status(201).json({data, url: "http://localhost:3002/payment"})
  }
  )    .catch((err) => {
      if (err instanceof ValidationError) {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });

}); */
  // 
router.get('/marchands/:id', (req, res) => {
  Transaction.findAll({
    where: { MarchandId: req.params.id }
  }
  ).then((data) => res.json(data))
  .catch((err) => res.sendStatus(500));
});

// GET
router.get("/:id", (req, res) => {
  Transaction.findByPk(req.params.id)
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
    .catch((err) => console.log(err) || res.sendStatus(500));
});


// PUT
router.put("/:id", (req, res) => {  
  Transaction.update(req.body, { where: { id: req.params.id }, returning: true })
    .then(([nbUpdated, result]) =>
      nbUpdated ? res.json(result[0]) : res.sendStatus(404)
    )
    .catch((err) => {
      if (err.name === "ValidationError") {
        res.status(400).json(prettifyValidationErrors(err.errors));
      } else {
        res.sendStatus(500);
      }
    });
});

router.delete("/:id", (req, res) => {
  Transaction.destroy({ where: { id: req.params.id } })
    .then((data) => (data ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => res.sendStatus(500));
});

module.exports = router;
