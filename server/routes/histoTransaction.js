const express = require("express");
const router = express.Router();
const { Transaction, Marchand, HistoTransaction } = require("../models/sequelize");

router.get('/transactions/marchands/:id', (req, res) => {
    HistoTransaction.findAll({
        where: { MarchandId: req.params.id }
      }
      ).then((data) => res.json(data))
      .catch((err) => res.sendStatus(500));
})

module.exports = router;
