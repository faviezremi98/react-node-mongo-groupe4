const mongoose = require("mongoose");

const Schema = new mongoose.Schema(
  {
    email: String
    },
  {
    collection: "Users"
  }
);

const User = mongoose.model("Users", Schema);

module.exports = User;
