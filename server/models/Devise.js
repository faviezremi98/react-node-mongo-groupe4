const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  devise: String,
  taux: Number,
  date: String
}, {
  collection: "Devise"
});

const model = new mongoose.model('Devise', schema);

module.exports = model;
