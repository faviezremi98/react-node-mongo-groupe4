const mongoose = require("mongoose");

const Schema = new mongoose.Schema(
  {
    // not sure about json type here
    email: String,
    denomSoc: String,
    KBIS: String,
    urlConf : String,
    urlAnnul: String,
    currency : String,
  },
  
  {
    collection: "Marchands"
  }
);

const Marchand = mongoose.model("Marchands", Schema);

module.exports = Marchand;
