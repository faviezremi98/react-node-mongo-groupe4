const db = require('../lib/db');
const mongoose = require("mongoose");
const Schema = new mongoose.Schema(
  {
    total: String,
    status: String,
    panier: Array
  },
  
  {collection: "Transactions"}
);

const Transaction = db.model("Transactions", Schema);

module.exports = Transaction;
