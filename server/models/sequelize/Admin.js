const connection = require("../../lib/sequelize");
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcryptjs");
class Admin extends Model { }

Admin.init(
  // Schema
  {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: { msg: "Email invalid" },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,

    }
  },
  // Options
  {
    sequelize: connection,
    modelName: "Admin"
  }
);


Admin.addHook("beforeCreate", async (admin, options) => {
  const salt = await bcrypt.genSalt(10);
  admin.password = await bcrypt.hash(admin.password, salt);
});

module.exports = Admin;
