const connection = require("../../lib/sequelize");
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcryptjs");
const Transaction = require("./Transaction");
class User extends Model {}

User.init(
  // Schema
  {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: { msg: "Email invalid" },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,

    },
    adresseFacturation: {
      type: DataTypes.STRING
    },
    adresseLivraison: {
      type: DataTypes.STRING
    }
  },
  // Options
  {
    sequelize: connection,
    modelName: "User"
  }
);


User.addHook("beforeCreate", async (user, options) => {
   const salt = await bcrypt.genSalt(10);
  user.password = await bcrypt.hash(user.password, salt); 
});

module.exports = User;
