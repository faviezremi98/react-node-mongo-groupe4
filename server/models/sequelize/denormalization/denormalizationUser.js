const UserPG = require("../User");
const UserMongo = require("../../User");

const denormalizeUser = async (user) => {
  await UserMongo.deleteOne({ id: user.id });
  const result = await UserPG.findOne({
    where: {
      id: user.id,
    }
  });
  const doc = new UserMongo(result);
  await doc.save();
};

module.exports = denormalizeUser;
