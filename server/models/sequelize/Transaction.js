const connection = require("../../lib/sequelize");
const { Model, DataTypes } = require("sequelize");
const { HistoTransaction } = require("./HistoTransaction");
 const Operation = require("./Operation");
 class Transaction extends Model {}

Transaction.init(
  // Schema
  {
    total: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    panier: {
      type: DataTypes.JSON,
      allowNull: true,
    } ,
    status: {
      // created , annulé, en attente, confirmé
      type: DataTypes.STRING  
    }
  },
  // Options
  {
    sequelize: connection,
    modelName: "Transaction",
  }
);


Transaction.hasMany(Operation);
Operation.belongsTo(Transaction); 

module.exports = Transaction;
