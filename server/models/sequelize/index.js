const sequelize = require("../../lib/sequelize");
const Transaction = require("./Transaction")
const Operation = require("./Operation")
const Marchand = require("./Marchand")
const Credential = require("./Credential")
const User = require("./User");
const Admin = require("./Admin");
const HistoTransaction = require('./HistoTransaction');
const denormalizeUser = require("./denormalization/denormalizationUser");

sequelize
  .sync({ alter: true })
  .then((result) => console.log("Sync OK"))
  .catch((result) => console.error("Sync KO", result));

User.addHook("afterCreate", async (user) => {
  denormalizeUser(user);
});
User.addHook("afterUpdate", async (user) => {
  denormalizeUser(user);
});
User.addHook("afterDestroy", async (user) => {
  denormalizeUser(user);
});

  
module.exports = {
  sequelize,
  Credential,
  Operation,
  Transaction,
  Marchand,
  User,
  Admin,
  HistoTransaction
};


