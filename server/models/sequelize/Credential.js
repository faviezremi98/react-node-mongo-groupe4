const connection = require("../../lib/sequelize");
const { Model, DataTypes } = require("sequelize");

class Credential extends Model {}

Credential.init(
  {
    // client_tocken et client_secret
    clientTocken: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    clientSecret: {
      type: DataTypes.STRING,
      allowNull: false,
    }
  },
  {
    sequelize: connection,
    modelName: "Credential",
  }
);

module.exports = Credential;
