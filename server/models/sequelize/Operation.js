const connection = require("../../lib/sequelize");
const { Model, DataTypes } = require("sequelize");

class Operation extends Model {}

Operation.init(
  // Schema
  {
    type: {
      type: DataTypes.STRING   
   },
    status: {
      type: DataTypes.STRING   
   }
  },
  // Options
  {
    sequelize: connection,
    modelName: "Operation",
  }
);

module.exports = Operation;
