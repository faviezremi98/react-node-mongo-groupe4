const connection = require("../../lib/sequelize");
const Credential = require("./Credential")
const Transaction = require("./Transaction")
const { Model, DataTypes } = require("sequelize");
const bcrypt = require("bcryptjs");
const HistoTransaction = require("./HistoTransaction");
class Marchand extends Model {}

Marchand.init(
  {
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: { msg: "Email invalid" },
      },
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    denomSoc: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    KBIS: {
      type: DataTypes.BLOB('long'),
      allowNull: false,
    },
    urlConf : {
      type: DataTypes.STRING,
      allowNull: false,
    },
    urlAnnul : {
      type: DataTypes.STRING,
      allowNull: false,
    },
    currency : {
      type: DataTypes.STRING,
      allowNull: false,
    },
    apiToken: {
      type: DataTypes.STRING,
      allowNull: true
    },
    apiSecret: {
      type: DataTypes.STRING,
      allowNull: true
    }
  },
  {
    sequelize: connection,
    modelName: "Marchand",
  }
);

Marchand.hasMany(Transaction);
Transaction.belongsTo(Marchand);

Marchand.hasMany(HistoTransaction);
HistoTransaction.belongsTo(Marchand);

Marchand.hasMany(Credential); 
Credential.belongsTo(Marchand);

Marchand.addHook("beforeCreate", async (marchand, options) => {
  const salt = await bcrypt.genSalt(10);
  marchand.password = await bcrypt.hash(marchand.password, salt);
});


module.exports = Marchand;
