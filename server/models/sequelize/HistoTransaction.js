const connection = require("../../lib/sequelize");
const { Model, DataTypes } = require("sequelize");
class HistoTransaction extends Model { }

HistoTransaction.init(
  // Schema
  {
    total: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    panier: {
      type: DataTypes.JSON,
      allowNull: true,
    },
    status: {
      type: DataTypes.STRING,
      allowNull: true
    }
  },
  // Options
  {
    sequelize: connection,
    modelName: "HistoTransaction",
  }
);


module.exports = HistoTransaction;
