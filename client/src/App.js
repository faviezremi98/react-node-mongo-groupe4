import React, { useState, useEffect } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Route,
  NavLink,
  Switch
} from "react-router-dom";
import Login from "./components/Login";
import Home from "./components/Home";
import Recipe from "./components/Recipe";
import Register from "./components/Register";
import Cart from "./components/Cart";
import ConfirmPayement from "./components/ConfirmPayement";
import AnnulPayement from "./components/AnnulPayement";

export default function App() {

  const [products, setProducts] = useState([]);
  const [total, setTotal] = useState(0);
  return (
    <section className="app">
      <Router>
        <div>
          <aside>
            <nav>
              <ul>
                <li>
                  <NavLink to="/home" activeClassName={"active"}>Home</NavLink>
                </li>
                {
                  window.sessionStorage.JWT === undefined ?
                    (
                      <>
                        <li>
                          <NavLink to="/login" activeClassName={"active"}>Login</NavLink>
                        </li>
                        <li>
                          <NavLink to="/register" activeClassName={"active"}>Register</NavLink>
                        </li>
                      </>
                    )
                    :
                    <>
                      <li>
                        <NavLink to="/cart" activeClassName={"active"}>Cart</NavLink>
                      </li>
                      <li>
                        <NavLink to="/login" activeClassName={"active"} onClick={(e) => {
                          e.preventDefault();
                          delete window.sessionStorage.JWT;
                          window.location.href = "/login";
                        }} >Log out</NavLink>
                      </li>
                    </>
                }
              </ul>
            </nav>
          </aside>
          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/home" component={() => <Home products={products} setProducts={setProducts} />} />
            <Route exact path="/" component={() => <Home products={products} setProducts={setProducts} />} />
            <Route exact path="/recipe/:id" component={() => <Recipe products={products} setProducts={setProducts} />} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/cart" component={() => <Cart products={products} setProducts={setProducts} setTotal={setTotal} total={total}/>} />
            <Route exact path="/confirm/:id" component={() => <ConfirmPayement total={total} setTotal={setTotal} />} />
            <Route exact path="/annul/:id" component={AnnulPayement} />
          </Switch>
        </div>
      </Router>
    </section>
  );
}
