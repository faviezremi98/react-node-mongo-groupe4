import React, { useEffect } from "react";
import Button from '@material-ui/core/Button';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { addTransaction } from "../context/actions/transactions";

const useStyles = makeStyles((theme) => ({
  /*     root: {
        '& .MuiTextField-root': {
          margin: theme.spacing(1),
          width: 200,
        },
      }, */
}));

const ColorButton = withStyles((theme) => ({
  root: {
    color: '#fafafa',
    backgroundColor: '#81c6b8',
    '&:hover': {
      backgroundColor: '#81c6b8',
    },
  },
}))(Button);


const Cart = ({ match, location, products, setProducts, total, setTotal }) => {
  const productsToBuy = products.length !== 0 ? products : JSON.parse(window.localStorage.products);
  let totalAmount = 0;

  useEffect(() => {
    productsToBuy.length !== 0 && productsToBuy.forEach(element => {
      totalAmount += parseFloat(element.price);
    });

    totalAmount = totalAmount.toFixed(2);
    setTotal(totalAmount);
  }, []);

  const handleSubmit = async (e) => {
    const panier = productsToBuy.map((obj) => (({price,title }) => ({price,title }))(obj))
    console.log(panier);    
    e.preventDefault();
    const res = await addTransaction(total,panier).then((res) => res); //au click sur le bouton payer coté site marchand on redirige vers
    window.location = res.url + "/" + btoa(res.data.id); //l'url du backoffice
  }

  return (
    <div className="block">
      <ul>
        {
          productsToBuy && productsToBuy.map((product) => {
            return <li>{product.price}€ {product.title}</li>
          })
        }
      </ul>
      <h2>Total Amount: {total}</h2>
      <ColorButton variant="contained" type="submit" onClick={handleSubmit} >Payer</ColorButton>
    </div>
  );
}

export default Cart;