import React, { useState, useEffect, useContext } from "react";
import { fetchRecipe }  from '../context/actions/recipes'
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ReactHtmlParser from 'react-html-parser'; 
import { config } from '../context/config';
import { Link } from "react-router-dom";
import { withRouter } from 'react-router';
const Recipe = ({match, location}) => {
    const recipeId = match.params.id;
    const [recipe, setRecipe] = useState([]);

    useEffect(() => {
      fetchRecipe(recipeId).then((data) =>  {
        setRecipe(data)
      }
      );
    }, []);
    console.log(recipe);
    let instructions = recipe.instructions;
    instructions = instructions !== undefined  ? instructions.replace('\n', '<br>') : instructions; 
    return (
      <>
       <h1>{recipe.title}</h1>
       {
        window.sessionStorage.JWT !== undefined ? 
        <div className="block">
          {/* <div>{recipe.dishTypes[0]}</div> */}
         <Grid container spacing={3}>
            <Grid item xs={6}>
              <img src={recipe.image} alt={recipe.title}/>
              <h3><i>{recipe.title}</i></h3>
              <ul>
                {
                  recipe.nutrition && recipe.nutrition.ingredients.map((element) => {
                    return <li><b>{element.amount}  {element.unit}</b>   {element.name}</li>
                  })
                }
              </ul>
            </Grid>
            <Grid item xs={6}>
              <h2>Instructions</h2>
              <ul>
                {recipe.analyzedInstructions && recipe.analyzedInstructions[0].steps.map((element) => {
                  return <li>{element.number}.  {element.step}</li>
                })}
              </ul>
              <Grid container justify="center" spacing={5}>
                <Grid item xs={6}>
                  <p>Health score: {recipe.healthScore}</p>
                  <p>Sans gluten: {recipe.glutenFree ? "Yes" : "No"}</p>
                  <p>Vegetarian: {recipe.vegetarian ? "Yes" : "No"}</p>
                  <p>Vegan: {recipe.vegan ? "Yes" : "No"}</p>
                </Grid>
                <Grid item xs={6}>
                  <p>Very Healthy: {recipe.veryHealthy ? "Yes" : "No"}</p>
                  <p>Very Popular: {recipe.veryPopular ? "Yes" : "No"}</p>
                  <p>Weight Watcher Smart points: {recipe.weightWatcherSmartPoints}</p>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12}>
            <p>{ReactHtmlParser (recipe.summary)}</p>
            </Grid>
         </Grid>
         
       </div> :
       <div className="block">
          <Link to="/login">Veuillez vous connecter pour pouvoir acheter ce contenu</Link>
       </div>
       }
       
      </>
    );
}

export default withRouter(Recipe);