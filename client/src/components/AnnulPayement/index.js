import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import CancelIcon from '@material-ui/icons/Cancel';
import { fetchTransactions } from "../context/actions/transactions";

const useStyles = makeStyles((theme) => ({
    h1: {
        color: "#81c6b8",
    },
    iconCheck: {
        fontSize: "10em",
        color: "red",
        display: "flex",
        margin: "auto"
    }
}));

const AnnulPayement = ({}) => {
    const classes = useStyles();

    const [transaction, setTransaction] = useState([]);

    const fetchTransactionsConfirm = async () => {
        const path = window.location.pathname.split('/');
        return await fetchTransactions(atob(path[2]));
    }

    useEffect(() => {
        fetchTransactionsConfirm().then(res => setTransaction(res));
    }, []);

    return (
        <div className="block">
            <h1 className={classes.h1}>Annulation de paiement</h1>
            <CancelIcon className={classes.iconCheck}></CancelIcon>
            <p>Votre paiement de : {transaction.total}€ a été annulé par vous même ou refusé par votre banque !</p>
        </div>
    )
};

export default AnnulPayement;
