import React, { useState, useEffect, useContext } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { fetchRecipes }  from '../context/actions/recipes'
import Cards from '../Cards';

const Home = ({products, setProducts}) => {
    const [recipes, setRecipes] = useState([]);

    useEffect(() => {
        fetchRecipes().then((data) =>  {
            setRecipes(data)
        }
        );
    }, []);

    return (
    <>
        <h1>Accueil</h1>
        <div className="block">
            <div className="row">
            {
                recipes.recipes && recipes.recipes.map((recipe) => 
                    <Cards recipe={recipe} products={products} setProducts={setProducts}/>
                )
            }
            </div>
        </div>
    </>
    )
    ;
}

export default Home;
//{products && products.map((element) => <Cards/>)}
