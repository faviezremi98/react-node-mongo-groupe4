import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Link } from "react-router-dom";

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
      width: '18%',
      margin: '1%',
    },
    media: {
      height: 140,
    },
  });

const Cards = ({recipe, products, setProducts}) => {
    const classes = useStyles();
    const url = "/recipe/"+recipe.id
    // On renvoie un nombre aléatoire entre une valeur min (incluse) 
// et une valeur max (exclue)

    recipe.price = Math.random() * (3 - 0.5) + 0.5
    recipe.price = recipe.price.toFixed(2);

    return (
      <Card className={classes.root}>
          <Link to={url}>
            <CardActionArea>
            <CardMedia
                className={classes.media}
                image={recipe.image}
                title={recipe.title}
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                {recipe.title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                {recipe.description}
                </Typography>
            </CardContent>
            </CardActionArea>
          </Link>
          <Button size="small" color="primary" onClick={(e)=> {
            e.preventDefault()
            setProducts([...products, recipe])
            window.localStorage.products = JSON.stringify([...products, recipe]);
          }}>
            Ajouter au Panier (+)
          </Button>
      </Card>
    );
}

export default Cards;