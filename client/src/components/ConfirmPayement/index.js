import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import { fetchTransactions } from "../context/actions/transactions";

const useStyles = makeStyles((theme) => ({
    h1: {
        color: "#81c6b8",
    },
    iconCheck: {
        fontSize: "10em",
        color: "#81c6b8",
        display: "flex",
        margin: "auto"
    }
}));

const ConfirmPayement = ({ }) => {
    const classes = useStyles();

    const [transaction, setTransaction] = useState([]);

    const fetchTransactionsConfirm = async () => {
        const path = window.location.pathname.split('/');
        return await fetchTransactions(atob(path[2]));
    }

    useEffect(() => {
        fetchTransactionsConfirm().then(res => setTransaction(res));
    }, []);

    console.log(transaction);

    return (
        <div className="block">
            <h1 className={classes.h1}>Confirmation de paiement</h1>
            <CheckCircleOutlineIcon className={classes.iconCheck}></CheckCircleOutlineIcon>
            <p>Votre paiement de : {transaction.total}€ a bien été confirmé. Merci de votre achat !</p>
        </div>
    )
};

export default ConfirmPayement;
