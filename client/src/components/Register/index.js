import React, { useState, useEffect, useContext } from "react";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { registerUser } from '../context/actions/security';
const useStyles = makeStyles((theme) => ({
    root: {
      '& .MuiTextField-root': {
        margin: theme.spacing(1),
        width: 200,
      },
    },
  }));

const ColorButton = withStyles((theme) => ({
    root: {
      color: '#fafafa',
      backgroundColor: '#81c6b8',
      '&:hover': {
        backgroundColor: '#81c6b8',
      },
    },
}))(Button);

const handleSubmit = async (e) => {
  e.preventDefault();
  const data = new FormData(e.currentTarget);
  const resp = await registerUser(data.get('email'), data.get('password')); 
  window.location.href = '/login';
}

const Register = ({}) => {

    const classes = useStyles();

    return (
        <div className="block login">
            <h1>Register</h1>
            <form onSubmit={handleSubmit}>
              <TextField id="outlined-basic" label="Email" variant="outlined" name="email"/>
              <TextField id="outlined-basic" label="Password" variant="outlined" type="password" name="password"/>
              <ColorButton variant="contained" type="submit">Register</ColorButton>
            </form>
            
        </div>
    )
}

export default Register;