export const fetchRecipes = () => {
    return fetch("https://api.spoonacular.com/recipes/random?apiKey=dacdf3b4a5c8425bae291d8166fffb2c&includeNutrition=true&number=15&tags=vegetarian,dessert,meal").then(
        (res) => res.json()
    ).then();
  };

export const fetchRecipe = (id) => {
    return fetch(`https://api.spoonacular.com/recipes/${id}/information?apiKey=dacdf3b4a5c8425bae291d8166fffb2c&includeNutrition=true`).then(
        (res) => res.json()
    ).then();
}