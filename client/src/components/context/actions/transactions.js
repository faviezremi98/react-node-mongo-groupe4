export const addTransaction = (total,panier) => {

  const credentials = {
    apiToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJmYXZpZXpAeW9wbWFpbC5jb20iLCJkZW5vbVNvYyI6InRlc3QiLCJpYXQiOjE1OTQ1NjI0MDksImV4cCI6MTU5NDU2NjAwOX0.hRMwoHYSUrHK-yteCyzGn3kO62lvDcg4qsGzPZ81jcI",
    apiSecret: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InJmYXZpZXpAeW9wbWFpbC5jb20iLCJkZW5vbVNvYyI6InRlc3QiLCJpYXQiOjE1OTQ1NjI0MDksImV4cCI6MTU5NDU2NjAwOX0.hRMwoHYSUrHK-yteCyzGn3kO62lvDcg4qsGzPZ81jcI",
  }
  return fetch('http://localhost:3000/transactions', {
    method: "POST",
    body: JSON.stringify({ total,panier}),
    headers: new Headers({
      "Content-Type": "application/json",
      Authorization: `Basic ${credentials.apiToken}:${credentials.apiSecret}`
    })
  }).then((res) => {
    return res.json();
  });
}

export const fetchTransactions = (id) =>
  fetch(`http://localhost:3000/transactions/${id}`, {
    method: "GET",
    headers: new Headers({
      Authorization: "Bearer "+window.sessionStorage.JWT
    })
  }).then((res) =>
    res.json()
  );
