export const checkLogin = (email, password) => {
    return fetch('http://localhost:3000/login_check', {
        method: "POST",
        body: JSON.stringify({email, password}),
        headers: {
            "Content-Type": "application/json"
          }
    }).then((res) => {
        return res.json();
    });
}

export const registerUser = (email, password) => {
    return fetch('http://localhost:3000/users', {
        method: "POST",
        body: JSON.stringify({email, password}),
        headers: {
            "Content-Type": "application/json"
        }
    }).then((res) => {
        return res.json()
    })
}