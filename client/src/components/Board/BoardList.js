import React, { useState, useEffect, useContext } from "react";
import Board from ".";
import BoardContext from "../context/BoardContext";

const BoardList = () => {
  const [selectedBoard, setSelectedBoard] = useState(null);
  const { selectors } = useContext(BoardContext);

  useEffect(() => {
    console.log("mount");
    setSelectedBoard(selectors.getBoards[0])
    return () => console.log("unmount");
  }, []);

  useEffect(() => {
    console.log("Updated", selectedBoard);

    return () => {
      console.log("beforeChange");
    };
  }, [selectedBoard]);

  return (
    <>
      {selectedBoard && <Board board={selectedBoard} />}
    </>
  );
};

export default BoardList;
