import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, TextField, Button, TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { fetchStatus , fetchCountTransactions} from '../../api/charts';
import { Animation } from '@devexpress/dx-react-chart';
import {
  Chart,
  BarSeries,
  Title,
  ArgumentAxis,
  ValueAxis,
} from '@devexpress/dx-react-chart-material-ui';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: "-30px",
        marginLeft: "250px",
        width: "calc(100% - 275px + 24px)"
    },
    paper: {
        height: "80vh",
        width: "60vw",
        marginTop: "10%",
        boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
    },
    table: {
        minWidth: 650,
    },
}));

const Charts = () => {
    const classes = useStyles();
         
   const [data, setData] = useState([]);
   useEffect(() => {
   fetchStatus().then((res) => { setData(res); console.log(res)});    
  }, []);
  const [count, setCount] = useState([]);
  useEffect(() => {
    fetchCountTransactions().then((res) => { setCount(res); console.log(res)});    
 }, []);
console.log(count);
   

    return (
         <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12}>
                <p> nombre total des transactions : {count} </p>
                <Grid container justify="center" spacing={2}>
                    <Grid key={0} item>
                        <Paper className={classes.paper}>
                         <Chart
                        data={data}
                        >
                        <ArgumentAxis />
                        <ValueAxis max={7} /> 
                        <BarSeries
                            valueField="count"
                            argumentField="_id"
                        />
                        <Title text="Nombre de transactions par status" />
                        <Animation />
                        </Chart> 
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid> 
    );
};

export default Charts;