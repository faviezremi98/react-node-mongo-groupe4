import React, { useState, useEffect } from "react";
import { withStyles, makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, TextField, Button, TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { fetchUsersActivation } from '../../api/fetchUsersActivation';
import { activateUser } from '../../api/activateUser';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: "-30px",
        marginLeft: "250px",
        width: "calc(100% - 275px + 24px)"
    },
    paper: {
        height: "80vh",
        width: "60vw",
        marginTop: "10%",
        boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
    },
    table: {
        minWidth: 650,
    },
}));

const ColorButton = withStyles((theme) => ({
    root: {
      color: '#fafafa',
      backgroundColor: '#0f2b38',
      '&:hover': {
        backgroundColor: '#81c6b8',
      },
    },
}))(Button);

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledTableRow = withStyles((theme) => ({
    root: {
      '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
      },
    },
  }))(TableRow);


const AcceptUsers = () => {
    const classes = useStyles();

    const [data, setData] = useState();

    useEffect(() => {
        fetchUsersActivation().then((resp) => {
            setData(resp);
        });
    }, []);
    
    const handleActivation = (e) => {
        e.preventDefault();
        var index = e.currentTarget.tabIndex;
        activateUser(data[index]);
    }
    console.log(data);
    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    <Grid key={0} item>
                        <Paper className={classes.paper}>
                            <TableContainer component={Paper}>
                                <Table className={classes.table} aria-label="caption table">
                                    <caption>Tableau pour accepter les nouveaux marchands</caption>
                                    <TableHead>
                                        <TableRow>
                                            <StyledTableCell>Dénomination Sociale</StyledTableCell>
                                            <StyledTableCell align="right">Email</StyledTableCell>
                                            <StyledTableCell align="right">Url Confirmation</StyledTableCell>
                                            <StyledTableCell align="right">Url Annulation</StyledTableCell>
                                            <StyledTableCell align="right">Devise</StyledTableCell>
                                            <StyledTableCell align="right">Actions</StyledTableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                        data && data.map((elem, index) => (
                                            <StyledTableRow>
                                                <StyledTableCell>{elem.denomSoc}</StyledTableCell>
                                                <StyledTableCell align="right">{elem.email}</StyledTableCell>
                                                <StyledTableCell align="right">{elem.urlConf}</StyledTableCell>
                                                <StyledTableCell align="right">{elem.urlAnnul}</StyledTableCell>
                                                <StyledTableCell align="right">{elem.currency}</StyledTableCell>
                                                <StyledTableCell align="right"><ColorButton onClick={handleActivation} key={index} id={elem.id}>Activer</ColorButton></StyledTableCell>
                                            </StyledTableRow>
                                        ))
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default AcceptUsers;