import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import {Grid, Paper,TableBody, TextField, Button, TableContainer, Table, TableHead, TableRow, TableCell } from '@material-ui/core';
import MaterialTable from 'material-table';
import { fetchAdminTransactions } from '../../api/fetchAdmin';

const useStyles = makeStyles((theme) => ({
  root: {
      flexGrow: 1,
      margin: "-30px",
      marginLeft: "250px",
      width: "calc(100% - 275px + 24px)"
  },
  paper: {
      height: "80vh",
      width: "60vw",
      marginTop: "10%",
      boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
  },
  table: {
      minWidth: 650,
  },
}));

const AdminTransactions = ({}) => {
  const classes = useStyles();

  const [transactions, setTransactions] = useState([]);

  const [state, setState] = useState({
    columns: [
      { title: 'Status', field: 'status' },
      { title: 'Total', field: 'total' },
    ],
  });
  useEffect(() => {
    fetchAdminTransactions().then((res) => { setTransactions(res); console.log(res)});

  }, []);
  const tran = transactions.map((obj) => (({ total, status }) => ({total, status }))(obj))

  return (

      <Grid container className={classes.root} spacing={3}>
      <Grid item xs={12}>
          <Grid container justify="center" spacing={2}>
              <Grid key={0} item>
                  <Paper className={classes.paper}>
                          <MaterialTable
                          title="Transactions"
                          columns={state.columns}
                          data = {tran}
                          />                                
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default AdminTransactions;