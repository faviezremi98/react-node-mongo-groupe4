import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, TextField, Button, Input, FormLabel } from '@material-ui/core';
import { fetchTransaction } from "../../api/fetchTransaction";
import { doPayment, changeStatus, histoLoading, histoConfirm } from "../../api/psp";
import { fetchMarchand } from "../../api/fetchMarchand";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({

    root: {
        flexGrow: 1,
        margin: "-30px"
    },
    paper: {
        height: "80vh",
        width: "40vw",
        marginTop: "10%",
        boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
    },
    control: {
        padding: theme.spacing(2),
    },
    h1: {
        textAlign: "center",
        position: "relative",
        top: "5%",
        fontSize: "2em"
    },
    form: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 500,
        },
        display: "flex",
        justifyContent: "center",
        top: "15%",
        position: "relative",
        alignItems: "center",

        '& .MuiInput-underline::before': {
            borderBottom: "none",
        },
    },
    div: {
        justifyContent: "center",
        display: "flex",
        flexDirection: "column"
    },
    button: {
        margin: "10% auto auto auto",
        width: 200,
    },
    inputSizeS: {
        width: "26.8ch!important"
    },
    h1Center: {
        display: "flex",
        justifyContent: 'center',
        top: '50%',
        position: 'relative'
    }
}));

const Transaction = ({ match, location }) => {
    const classes = useStyles();
    const [data, setData] = useState({ transaction: null, marchand: null, idHisto: null });
    const handleSubmit = async (e) => {
        const resp = await doPayment(data.transaction).then(async (respData) => {
            const histo = await histoLoading(data.transaction).then((data) => {
                setData({ transaction: respData, marchand: data.marchand, idHisto: data.id+1 });
            });
        }); // j'effectue le changement de statut
    };

    useEffect(async () => {
        const resp = await fetchTransaction(atob(match.params.id)); // je récupère ma transaction
        const result = await fetchMarchand(resp.MarchandId);
        setData({ marchand: result, transaction: resp });
    }, []);

    console.log(data.idHisto);
    useEffect( () => {
        if (data.transaction !== null && data.transaction.status === "En cours") //onloading, je change le statut en confirmed et je redirige vers l'url du marchand
        {
            setTimeout(async () => {
                const resp = await changeStatus(data.transaction);
                const histo = await histoConfirm(data.transaction, data.idHisto);
                window.location = resp.marchand.urlConf + "/" + btoa(data.transaction.id);
            }, 2000)
        }
    }, [data]);
    

    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    <Grid key={0} item>
                        {data.transaction !== null && data.transaction.status === "En cours" // SI changement de statut, j'affiche loading...
                            ?
                            <Paper className={classes.paper}>
                                <h1 className={classes.h1Center}>Loading...</h1>
                            </Paper>
                            :
                            <Paper className={classes.paper}>
                                <h1 className={classes.h1}>Paiement</h1>
                                <form onSubmit={handleSubmit} method="GET" className={classes.form} noValidate autoComplete="off">
                                    <div className={classes.div}>
                                        <TextField id="outlined-basic" label="Numéro de Carte" variant="outlined" name="numCart" />
                                        <TextField id="outlined-basic" label="Nom Propriétaire de la Carte" variant="outlined" name="nomCart" />
                                        <div>
                                            <TextField className={classes.inputSizeS} id="outlined-basic" label="Date d'invalidatité de la Carte" InputLabelProps={{ shrink: true }} placeholder="jj/mm/aaaa" variant="outlined" name="datefin" type="date" />
                                            <TextField className={classes.inputSizeS} id="outlined-basic" InputLabelProps={{ shrink: true }} placeholder="CVC" label="CVC" variant="outlined" name="cvc" />
                                        </div>
                                        <Button type="submit" className={classes.button} variant="contained" color="primary">Payer</Button>
                                        <Button className={classes.button} variant="contained" color="primary" onClick={() => {
                                            window.location = data.marchand.urlAnnul;
                                        }}>Annuler</Button>
                                    </div>
                                </form>
                            </Paper>
                        }

                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )
};

export default Transaction;