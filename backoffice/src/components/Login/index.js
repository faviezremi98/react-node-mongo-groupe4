import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, TextField, Button } from '@material-ui/core';
import { checkLogin } from "../../api/security"

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: "-30px"
    },
    paper: {
        height: "80vh",
        width: "40vw",
        marginTop: "10%",
        boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
    },
    control: {
        padding: theme.spacing(2),
    },
    h1: {
        textAlign: "center",
        position: "relative",
        top: "5%",
        fontSize: "2em"
    },
    form: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 500,
        },
        display: "flex",
        justifyContent: "center",
        top: "30%",
        position: "relative",
        alignItems: "center"
    },
    div: {
        justifyContent: "center",
        display: "flex",
        flexDirection: "column"
    },
    button: {
        margin: "auto",
        width: 200
    }
}));

const Login = ({marchand, setMarchand}) => {
    const classes = useStyles();

    const handleSubmit = (e) => {
        e.preventDefault();
        const data = new FormData(e.currentTarget);
        loginCheck(data);
    }

    const loginCheck = async (data) => {
        
        const res = await checkLogin(data.get('email'), data.get('password'));
        if (res.token !== undefined) {
            window.sessionStorage.setItem('email',data.get('email'))
            window.sessionStorage.JWT = res.token;
            res.admin !== undefined ? window.sessionStorage.isAdmin = true : window.sessionStorage.isAdmin = false;
            setMarchand(res.data);
            window.sessionStorage.isAdmin !== true ?
            window.location.href = "/marchand-transactions"
            : window.location.href = "/accept-users";
        }        
    };

    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    <Grid key={0} item>
                        <Paper className={classes.paper}>
                            <h1 className={classes.h1}>Login</h1>
                            <form className={classes.form} autoComplete="off" onSubmit={handleSubmit} method="POST">
                                <div className={classes.div}>
                                    <TextField id="outlined-basic" label="Email" variant="outlined" name="email" />
                                    <TextField id="outlined-basic" label="Password" variant="outlined" type="password" name="password" />
                                    <Button className={classes.button} variant="contained" color="primary" type="submit">Login</Button>
                                </div>
                            </form>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Login;