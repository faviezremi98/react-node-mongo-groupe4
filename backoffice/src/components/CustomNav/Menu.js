import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { NavLink } from "react-router-dom";
import TollIcon from '@material-ui/icons/Toll';
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles({
  list: {
    width: "250px",
  },
  fullList: {
    width: 'auto',
  },
  item: {
    justifyContent: "center",
    color: "white",
  },
  firstItem: {
    fontSize: "5em"
  },
  drawer: {
    '& .MuiDrawer-paper': {
      backgroundColor: "#0f2b38", 
    }
  }
});

export default function Menu({ toggleDrawer, openState }) {
  const classes = useStyles();
  return (
    <div>
      <>
        <Drawer variant="permanent" className={classes.drawer}>
          <List className={classes.list}>
            <ListItem className={classes.item}><TollIcon className={classes.firstItem} /></ListItem>
            <Divider />
            {
              window.sessionStorage.JWT !== undefined && window.sessionStorage.isAdmin === "false" ?
                <>
                  <NavLink to="/marchand-transactions">
                    <ListItem button key="marchandTransactions" className={classes.item} onClick={toggleDrawer(false)}>
                      Gérer mes transactions             

                    </ListItem>
                  </NavLink>


                  <NavLink to="/informations">
                    <ListItem button key="marchandTransactions" className={classes.item} onClick={toggleDrawer(false)}>
                      Mes informations
                    </ListItem>
                  </NavLink>
                  <NavLink to="/credential">
                    <ListItem button key="credential" className={classes.item} onClick={toggleDrawer(false)}>
                      Gérer mes credendials           
                    </ListItem>
                  </NavLink>
                  <NavLink to="/login">
                    <ListItem button key="register" className={classes.item} onClick={() => {
                        delete window.sessionStorage.JWT;
                        delete window.sessionStorage.isAdmin;
                        toggleDrawer(false)
                      }}>
                      Logout
                    </ListItem>
                  </NavLink>
                </> : window.sessionStorage.JWT !== undefined && window.sessionStorage.isAdmin === "true" ?
                  <>
                    <NavLink to="/accept-users">
                      <ListItem button key="login" className={classes.item} onClick={toggleDrawer(false)}>
                        Accepter les utilisateurs
                    </ListItem>
                    </NavLink>
                    <NavLink to="/charts">
                    <ListItem button key="marchandTransactions" className={classes.item} onClick={toggleDrawer(false)}>
                  Charts
                    </ListItem>
                  </NavLink>
                    <NavLink to="/admin-transactions">
                    <ListItem button key="marchandTransactions" className={classes.item} onClick={toggleDrawer(false)}>
                      Gérer mes transactions  
                    </ListItem>
                  </NavLink>
                  <NavLink to="/marchands">
                    <ListItem button key="marchands" className={classes.item} onClick={toggleDrawer(false)}>
                      Marchands
                    </ListItem>
                  </NavLink>
                    <NavLink to="/logout" onClick={() => {
                        delete window.sessionStorage.JWT;
                        delete window.sessionStorage.isAdmin;
                        window.location.href = '/login'
                      }}>
                      <ListItem button key="register" className={classes.item}>
                        Logout
                    </ListItem>
                    </NavLink>
                  </> :
                  <>
                    <NavLink to="/login">
                      <ListItem button key="login" className={classes.item} onClick={toggleDrawer(false)}>
                        Login
                  </ListItem>
                    </NavLink>
                    <NavLink to="/register">
                      <ListItem button key="register" className={classes.item} onClick={toggleDrawer(false)}>
                        register
                  </ListItem>
                    </NavLink>
                  </>
            }
          </List>
        </Drawer>
      </>
    </div>
  );
}
