import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Menu from './Menu';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import {
  Route,
  Link
} from "react-router-dom";
import Login from "../Login";
import Register from "../Register";
import Transactions from "../Transactions";
import AcceptUsers from '../Admin/AcceptUsers';
import Charts from '../Admin/Charts';
import Transaction from '../Marchand/Transaction'
import Credential from '../Marchand/Credential'
import Marchands from '../Admin/Marchands'
import Information from '../Marchand/Informations'
import AdminTransactions from '../Admin/Transaction';
import { checkMarchand } from '../../api/fetchMarchand'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    '& .MuiAppBar-root': {
      backgroundColor: "#0f2b38",
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  linkAppBar: {
    color: "white!important",
    textDecoration: "none",
  }
}));

export default function CustomNav({ }) {
  const classes = useStyles();
  const [menu, setMenu] = React.useState(false);
  const [data, setData] = useState();

  const toggleDrawer = (bool) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setMenu(bool);
  };

  useEffect(() => {
    (async () => {
        await checkMarchand(window.sessionStorage.email).then(res => setData(res))
    })();
}, [])

  var currentRoute = window.location.pathname;
  return (
    <div className={classes.root}>
      {
        !currentRoute.startsWith("/payment") ?
          <>
            <AppBar position="static">
              <Toolbar>
                <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" >
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                  Strype
                </Typography>
                {window.sessionStorage.JWT !== undefined ?
                  <Link onClick={() => {
                    delete window.sessionStorage.JWT;
                    delete window.sessionStorage.isAdmin;
                    window.location.href = '/login'
                  }} className={classes.linkAppBar}>Logout <AccountCircleIcon /> </Link>
                  :
                  <Link to="/login" className={classes.linkAppBar}>Login <AccountCircleIcon /> </Link>}

              </Toolbar>
            </AppBar>
            <Menu
              toggleDrawer={toggleDrawer}
              openState={menu}
            >
            </Menu>
          </>
          : <></>
      }

      <Route exact path="/login" component={() => <Login marchand={data} setMarchand={setData} />} />
      {/* <Route exact path="/home" component={() => <Home products={products} setProducts={setProducts} />} />
        <Route exact path="/" component={() => <Home products={products} setProducts={setProducts} />} /> */}
      <Route exact path="/register" component={Register} />
      <Route exact path="/accept-users" component={AcceptUsers} />
      <Route exact path="/charts" component={Charts} />
      <Route exact path="/marchand-transactions" component={() => <Transaction marchand={data} />} />
      <Route exact path="/admin-transactions" component={() => <AdminTransactions marchand={data} />} />
      <Route exact path="/informations" component={() => <Information marchand={data} />} />
      <Route exact path="/credential" component={() => <Credential marchand={data} />} />
      {/* <Route exact path="/marchand-transactions" component={() => <Transaction marchand={data} />} /> */}
      <Route exact path="/payment/:id" component={Transactions} />
      <Route exact path="/marchands" component={() => <Marchands marchand={data} setMarchand={setData}/>} />
      <Route exact path="/marchands/:id" />

    </div>
  );
}