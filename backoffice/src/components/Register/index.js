import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, TextField, Button, Input, FormLabel, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { registerMarchand } from "../../api/security"
import { emailActivate } from "../../api/email";

const useStyles = makeStyles((theme) => ({

    root: {
        flexGrow: 1,
        margin: "-30px"
    },
    paper: {
        height: "80vh",
        width: "40vw",
        marginTop: "10%",
        boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
    },
    control: {
        padding: theme.spacing(2),
    },
    h1: {
        textAlign: "center",
        position: "relative",
        top: "5%",
        fontSize: "2em"
    },
    form: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 500,
        },
        display: "flex",
        justifyContent: "center",
        top: "15%",
        position: "relative",
        alignItems: "center",

        '& .MuiInput-underline::before': {
            borderBottom: "none",
        },
    },
    div: {
        justifyContent: "center",
        display: "flex",
        flexDirection: "column"
    },
    button: {
        margin: "10% auto auto auto",
        width: 200,
    },
    inputFile: {
        marginTop: "5%"
    }
}));


const Register = ({ }) => {
    const classes = useStyles();
    const [currency, setCurrency] = useState();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = new FormData(e.currentTarget);
        let tmpFile = data.get('KBIS');
        toBase64(tmpFile).then(async (reader) => {
            data.set('KBIS', reader.result);
            let obj = {};
            data.forEach((element, key) => {
                obj[key] = element;
            })

            await registerMarchand(obj).then(async (data) => {
                const msg = await emailActivate(data.id).then(() => {
                    window.location.href = "/login"
                });
            });
        });

    }

    const toBase64 = (file) => {
        return new Promise(function (resolve) {
            var reader = new FileReader();
            reader.onloadend = function () {
                resolve(reader);
            }
            reader.readAsDataURL(file);
        })
    }

    const handleChangeCurrency = (event) => {
        setCurrency(event.target.value);
      };

    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    <Grid key={0} item>
                        <Paper className={classes.paper}>
                            <h1 className={classes.h1}>Register</h1>
                            <form onSubmit={handleSubmit} method="POST" className={classes.form} noValidate autoComplete="off">
                                <div className={classes.div}>
                                    <TextField id="outlined-basic" label="Dénomination Sociale" variant="outlined" name="denomSoc" />
                                    <TextField id="outlined-basic" label="Email" variant="outlined" name="email" />
                                    <TextField id="outlined-basic" label="Mot de passe" variant="outlined" name="password" type="password" />
                                    <TextField id="outlined-basic" label="Url du site de confirmation de paiement" variant="outlined" name="urlConf" />
                                    <TextField id="outlined-basic" label="Url du site d'annulation de paiement" variant="outlined" name="urlAnnul" />
                                    <FormControl variant="outlined" className={classes.formControl}>
                                        <InputLabel id="demo-simple-select-outlined-label">Devises</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-outlined-label"
                                            id="demo-simple-select-outlined"
                                            value={currency}
                                            onChange={handleChangeCurrency}
                                            label="Devise"
                                            name="currency"
                                        >
                                            <MenuItem value="">
                                                <em></em>
                                            </MenuItem>
                                            <MenuItem value={"USD"}>USD</MenuItem>
                                            <MenuItem value={"EUR"}>EUR</MenuItem>
                                            <MenuItem value={"JPY"}>JPY</MenuItem>
                                            <MenuItem value={"GBP"}>GBP</MenuItem>
                                            <MenuItem value={"CHF"}>CHF</MenuItem>
                                            <MenuItem value={"CAD"}>CAD</MenuItem>
                                        </Select>
                                    </FormControl>                                    <FormLabel className={classes.inputFile} >KBIS</FormLabel>
                                    <Input type="file" name="KBIS" accept=".png,.jpeg,.jpg,.pdf" />
                                    <Button type="submit" className={classes.button} variant="contained" color="primary">Register</Button>
                                </div>
                            </form>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default Register;