import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, FormLabel, Input, TextField, Button, Link } from '@material-ui/core';
import { updateMarchandApiKey, fetchMarchand } from '../../api/fetchMarchand'
import { fetchMarchandsAsAdmin } from "../../api/fetchMarchandsAsAdmin";
import { checkMarchand } from '../../api/fetchMarchand'

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: "-30px",
        marginLeft: "250px",
        width: "calc(100% - 275px + 24px)"
    },
    paper: {
        height: "80vh",
        width: "60vw",
        marginTop: "10%",
        boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
    },
    table: {
        minWidth: 650,
    },

    control: {
        padding: theme.spacing(2),
    },
    h1: {
        textAlign: "center",
        position: "relative",
        top: "5%",
        fontSize: "2em"
    },
    form: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 500,
        },
        display: "flex",
        justifyContent: "center",
        top: "15%",
        position: "relative",
        alignItems: "center",

        '& .MuiInput-underline::before': {
            borderBottom: "none",
        },
    },
    div: {
        justifyContent: "center",
        display: "flex",
        flexDirection: "column"
    },
    button: {
        margin: "10% auto auto auto",
        width: 200,
    },
    inputFile: {
        marginTop: "5%"
    }
}));


const Credential = ({ marchand, setMarchand }) => {
    const classes = useStyles();
    const [data, setUpdated] = useState({ marchand, updated: false });

    const handleSubmit = async (e) => {
        await updateMarchandApiKey(marchand, marchand.id).then((res) => {
            setUpdated({marchand:res, updated: true});
        });
    }

    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    <Grid key={0} item>
                        <Paper className={classes.paper}>
                            <form onSubmit={handleSubmit} method="PUT" className={classes.form} noValidate autoComplete="off">
                                <div className={classes.div}>
                                    <TextField id="outlined-basic" InputProps={{ readOnly: true }} label="api Token" variant="outlined" defaultValue={data.marchand !== undefined ? data.marchand.apiToken : null} name="apiToken" />
                                    <TextField id="outlined-basic" InputProps={{ readOnly: true }} aria-readonly={true} label="api Secret" variant="outlined" defaultValue={data.marchand !== undefined ? data.marchand.apiToken : null} name="apiSecret" />
                                    {
                                        data.updated ? <h2>Les tokens ont bien été mis à jour !</h2>: null
                                    }
                                    <Button type="submit" className={classes.button} variant="contained" color="primary">recharger</Button>
                                </div>
                            </form>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>

    );
};

export default Credential;
