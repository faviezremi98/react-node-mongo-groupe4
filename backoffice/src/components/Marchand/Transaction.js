import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper, TableBody, TextField, Button, TableContainer, Table, TableHead, TableRow, TableCell } from '@material-ui/core';
import MaterialTable from 'material-table';
import { fetchMarchandTransactions } from '../../api/fetchMarchand';
import { remboursement } from "../../api/psp";
import { fetchTransaction } from "../../api/fetchTransaction";
import { fetchDevises } from "../../api/fetchDevises";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    margin: "-30px",
    marginLeft: "250px",
    width: "calc(100% - 275px + 24px)"
  },
  paper: {
    height: "80vh",
    width: "60vw",
    marginTop: "10%",
    boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
  },
  table: {
    minWidth: 650,
  },
  divContainer: {
    padding: "2%"
  },
  div: {
    display: "flex",
    justifyContent: "space-evenly"
  },
  ul: {
    listStyle: "none"
  },
  li: {
    margin: "2% auto"
  }
}));

const Transaction = ({ marchand }) => {
  const classes = useStyles();

  const [transactions, setTransactions] = useState([]);
  const [updated, setUpdated] = useState(false);
  const [devises, setDevises] = useState([]);
  const [state, setState] = useState({
    columns: [
      { title: 'Id', field: 'id' },
      { title: 'Status', field: 'status' },
      { title: 'Total', field: 'total'},
    ],
    devises: []
  });

  useEffect(() => {
    fetchMarchandTransactions(marchand !== undefined ? marchand.id : null).then((res) => {
      setTransactions(res);
    }
    );
  }, [marchand, updated]);

  useEffect(() => {
    fetchDevises().then((data) => {
      setState({
        columns: [
          { title: 'Id', field: 'id' },
          { title: 'Status', field: 'status' },
          { title: 'Total', field: 'total', render: rowData => {
            let total = rowData.total;
            total = data.length !== 0 ? (total + '€ - ' + (data.devise.taux * total).toFixed(2))+ ' ' + data.devise.devise : total;
            return total;
          } },
        ],
        devises: data
      });
    });
  }, [])

  useEffect(() => {
    setUpdated(false);
  }, [updated])

  const handleRemboursement = async (e) => {
    const transaction = await fetchTransaction(e.currentTarget.id);
    const resp = await remboursement(transaction).then(res => setUpdated(true));
  };

  const tran = transactions && transactions.map((obj) => (({ total, status, id, panier }) => ({ total, status, id, panier }))(obj))
  console.log(devises);
  return (

    <Grid container className={classes.root} spacing={3}>
      <Grid item xs={12}>
        <Grid container justify="center" spacing={2}>
          <Grid key={0} item>
            <Paper className={classes.paper}>
              <MaterialTable
                title="Transactions"
                columns={state.columns}
                data={tran}
                detailPanel={rowData => {
                  return (
                    <div className={classes.divContainer}>
                    <h2>Panier: </h2>
                    <div className={classes.div}>
                      <ul className={classes.ul}>
                        {
                          rowData.panier.map((element) => 
                          (
                          <>
                          <li className={classes.li}>{element.price}€ - {element.title}</li>
                            <li>{(element.price * state.devises.devise.taux).toFixed(2)} {state.devises.devise.devise} </li>
                            </>))
                        }
                      
                      </ul>
                      {
                          rowData.status === 'confirmed' ? 
                          <Button onClick={handleRemboursement} id={rowData.id} className={classes.button} variant="contained" color="primary" type="submit">Rembourser</Button>
                          : null
                        }
                    </div>
                    </div>
                  )
                }}
              />
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Transaction;