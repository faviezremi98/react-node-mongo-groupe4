import React, { useState, useEffect } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Paper,FormLabel,Input, TextField, Button, Link} from '@material-ui/core';
import { updateMarchand } from '../../api/fetchMarchand'
const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        margin: "-30px",
        marginLeft: "250px",
        width: "calc(100% - 275px + 24px)"
    },
    paper: {
        height: "80vh",
        width: "60vw",
        marginTop: "10%",
        boxShadow: "0px 4px 20px 0px rgba(0,0,0,0.2), 0px 1px 20px 0px rgba(0,0,0,0.14), 1px 2px 5px 2px rgba(0,0,0,0.12)"
    },
    table: {
        minWidth: 650,
    },

    control: {
        padding: theme.spacing(2),
    },
    h1: {
        textAlign: "center",
        position: "relative",
        top: "5%",
        fontSize: "2em"
    },
    form: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 500,
        },
        display: "flex",
        justifyContent: "center",
        top: "15%",
        position: "relative",
        alignItems: "center",

        '& .MuiInput-underline::before': {
            borderBottom: "none",
        },
    },
    div: {
        justifyContent: "center",
        display: "flex",
        flexDirection: "column"
    },
    button: {
        margin: "10% auto auto auto",
        width: 200,
    },
    inputFile: {
        marginTop: "5%"
    }
}));


const Information = ({marchand}) => {
    const classes = useStyles();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = new FormData(e.currentTarget);
        let obj = {};
        data.forEach((element, key) => {
            obj[key] = element;
        })
      const a = updateMarchand(obj,marchand.id);
    }

    return (
        <Grid container className={classes.root} spacing={3}>
            <Grid item xs={12}>
                <Grid container justify="center" spacing={2}>
                    <Grid key={0} item>
                        <Paper className={classes.paper}>
                        <form onSubmit={handleSubmit} method="PUT" className={classes.form} noValidate autoComplete="off">
                             <div className={classes.div}>
                            <TextField id="outlined-basic" label="Email" variant="outlined" defaultValue= {marchand.email} name="email" />
                            <TextField id="outlined-basic" label="Mot de passe" variant="outlined" name="password" type="password"/>
                            <TextField id="outlined-basic" label="Dénomination Sociale" variant="outlined"  defaultValue= {marchand.denomSoc} name="denomSoc" />
                            <TextField id="outlined-basic" label="Url du site de confirmation de paiement" variant="outlined" defaultValue= {marchand.urlConf} name="urlConf" />
                            <TextField id="outlined-basic" label="Url du site d'annulation de paiement" variant="outlined" defaultValue= {marchand.urlAnnul}name="urlAnnul" />
                            <TextField id="outlined-basic" label="Devise" variant="outlined" defaultValue= {marchand.currency} name="currency" />
{/*                             <FormLabel id="filled-formLabel"className={classes.inputFile} variant="filled" >KBIS</FormLabel>
                             <Input type="file" name="KBIS" accept=".png,.jpeg,.jpg,.pdf" />     */}                      
                             <Link> Mon fichier x</Link>
                            <Button type="submit" className={classes.button} variant="contained" color="primary">Modifier</Button>
                            </div>
                            </form>
                        </Paper>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Information;