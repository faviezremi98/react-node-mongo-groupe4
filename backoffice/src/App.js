import React, { useState, useEffect } from 'react';
import './App.css';
import {
  BrowserRouter as Router,
} from "react-router-dom";
import CustomNav from "./components/CustomNav";

function App() {

  return (
    <section className="app">
      <Router>
        <CustomNav/>
      </Router>
    </section>
  );
}

export default App;
