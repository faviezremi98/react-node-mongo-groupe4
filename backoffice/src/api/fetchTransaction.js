export const fetchTransaction = (id) => {
    const credentials = {
        apiToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJkZW5vbVNvYyI6InRlc3QiLCJpYXQiOjE1OTQ0ODk1MzMsImV4cCI6MTU5NDQ5MzEzM30.mtMEErmaNqGGI1vRWiWDzf4ZrKWKZT0AtfaZijkuHNg",
        apiSecret: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RAdGVzdC5jb20iLCJkZW5vbVNvYyI6InRlc3QiLCJpYXQiOjE1OTQ0ODk1MzMsImV4cCI6MTU5NDQ5MzEzM30.mtMEErmaNqGGI1vRWiWDzf4ZrKWKZT0AtfaZijkuHNg",
    }
    return fetch(`http://localhost:3000/transactions/${id}`, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': "application/json",
            Authorization: `Basic ${credentials.apiToken}:${credentials.apiSecret}`
        })
    }).then((resp) => {
        return resp.json();
    }
    )
};


