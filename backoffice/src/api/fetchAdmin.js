export const fetchAdminTransactions = () => {
  return fetch(`http://localhost:3000/transactions`, {
      method: "GET",
      headers: new Headers({
          "Authorization": "Bearer "+window.sessionStorage.JWT,
      })
  }).then((resp) => resp.json());
}




export const fetchAdminMarchands = () => {
  return fetch('http://localhost:3000/marchands', {
      method: "GET",
      headers: new Headers({
          "Authorization": "Bearer "+window.sessionStorage.JWT,
      })
  }).then((resp) => {
      return resp.json();
  });
}