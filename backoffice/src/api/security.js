export const registerMarchand = (user) => {

    return fetch('http://localhost:3000/marchands', {
        method: "POST",
        body: JSON.stringify(user),
        headers: {
            "Content-Type": "application/json"
        }
    }).then((res) => {
        return res.json()
    })
}

export const checkLogin = (email, password) => {
    return fetch('http://localhost:3000/login_check', {
        method: "POST",
        body: JSON.stringify({email, password, marchand: true}),
        headers: {
            "Content-Type": "application/json"
          }
    }).then((res) => {
        return res.json();
    });
}
