export const doPayment = (transaction) => {
    return fetch('http://localhost:3000/payment/', {
        method: "POST",
        body: JSON.stringify(transaction),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res => res.json());
}

export const changeStatus = (transaction) => {
    return fetch('http://localhost:3000/payment/change', {
        method: "POST",
        body: JSON.stringify(transaction),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res => res.json());
}

export const histoLoading = (transaction) => {
    return fetch('http://localhost:3000/payment/histo/loading', {
        method: "POST",
        body: JSON.stringify({
            "MarchandId": transaction.MarchandId,
            "panier": transaction.panier,
            "status": transaction.status,
            "total": transaction.total 
        }),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res =>  res.json());
}

export const histoConfirm = (transaction, idHisto) => {
    return fetch('http://localhost:3000/payment/histo/confirm', {
        method: "POST",
        body: JSON.stringify({
            "MarchandId": transaction.MarchandId,
            "panier": transaction.panier,
            "status": transaction.status,
            "total": transaction.total,
            "id": idHisto
        }),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res =>  res.json());
}

export const remboursement = (transaction) => {
    return fetch('http://localhost:3000/payment/remboursement', {
        method: "POST",
        body: JSON.stringify(transaction),
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res => res.json());
}