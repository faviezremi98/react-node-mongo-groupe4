export const activateUser = (user) => {
    console.log(user);
    return fetch(`http://localhost:3000/marchands/activate/${user.id}`,{
        method: 'PUT',
        headers: new Headers({
            "Authorization": "Bearer "+window.sessionStorage.JWT,
            "Content-Type": "application/json"
        }),
        body: JSON.stringify(user),
    })
};