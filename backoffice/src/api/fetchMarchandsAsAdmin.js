export const fetchMarchandsAsAdmin = (id) => {
    return fetch(`http://localhost:3000/admins/marchands-by-admin/${id}`, {
        method: "GET",
        headers: new Headers({
            Authorization: "Bearer "+window.sessionStorage.JWT
        })
    }).then(
        res => res.json()
    )
}