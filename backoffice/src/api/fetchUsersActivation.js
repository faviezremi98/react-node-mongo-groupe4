export const fetchUsersActivation = () => {
    return fetch('http://localhost:3000/marchands/activation', {
        method: "GET",
        headers: new Headers({
            "Authorization": "Bearer "+window.sessionStorage.JWT,
        })
    }).then((resp) => {
        return resp.json();
    });
}

