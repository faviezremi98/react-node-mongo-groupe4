export const fetchDevises = () => {
    return fetch('http://localhost:3000/devises/', {
        method: "GET",
        headers: new Headers({
            Authorization: "Bearer "+window.sessionStorage.JWT
        })
    }).then(res => res.json());
}