export const fetchStatus = () => {
    return fetch('http://localhost:3000/transactions/status', {
        method: "GET",
        headers: new Headers({
            "Authorization": "Bearer "+window.sessionStorage.JWT,
        })
    }).then((resp) => {
        return resp.json();
    });
}

export const fetchCountTransactions = () => {
    return fetch('http://localhost:3000/transactions/count', {
        method: "GET",
        headers: new Headers({
            "Authorization": "Bearer "+window.sessionStorage.JWT,
        })
    }).then((resp) => {
        return resp.json();
    });
}

