export const fetchMarchand = (id) => {
    const credentials = {
        apiToken: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImZhdmllei5yZW1pQG91dGxvb2suZnIiLCJkZW5vbVNvYyI6ImZhdmllei5yZW1pQG91dGxvb2suZnIiLCJpYXQiOjE1OTM5ODA0ODMsImV4cCI6MTU5Mzk4NDA4M30.mTMVLW4qxO4jogjaZKdtyrNV_YfIOAfKp-GsUlTOE9g",
        apiSecret: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiZW1haWwiOiJmYXZpZXoucmVtaUBvdXRsb29rLmZyIiwiZGVub21Tb2MiOiJmYXZpZXoucmVtaUBvdXRsb29rLmZyIiwiaWF0IjoxNTkzOTgwNDgzLCJleHAiOjE1OTM5ODQwODN9.qtWm-74ZJKYvdW7ArS1KvpEj3fsGqZEreeikrVWs8CI",
    }
    return fetch(`http://localhost:3000/marchands/${id}`, {
        method: 'GET',
        headers: new Headers({
            'Content-Type': "application/json",
            Authorization: `Basic ${credentials.apiToken}:${credentials.apiSecret}`
        })
    }).then(res => res.json())
}
export const fetchMarchandTransactions = (id) => {
  return fetch(`http://localhost:3000/transactions/marchands/${id}`, {
      method: "GET",
      headers: new Headers({
          "Authorization": "Bearer "+window.sessionStorage.JWT,
      })
  }).then((resp) => resp.json());
}


export const checkMarchand = (email) => {
  return fetch(`http://localhost:3000/marchands/email/${email}`, {
      method: "GET",
      headers: new Headers({
          "Authorization": "Bearer "+window.sessionStorage.JWT,
      })
  }).then((resp) => resp.json());
}


export const updateMarchand = (data,id) => {
  console.log(data);
  return fetch(`http://localhost:3000/marchands/${id}`,{
      method: 'PUT',
      body: JSON.stringify(data),
      headers: new Headers({
          "Authorization": "Bearer "+window.sessionStorage.JWT,
          "Content-Type": "application/json"
      }),
  })
};

export const updateMarchandApiKey = (data, id) => {
    console.log(data);
    return fetch(`http://localhost:3000/marchands/apiKey/${id}`, {
        method:'PUT',
        body: JSON.stringify(data),
        headers: new Headers({
            "Authorization": "Bearer "+window.sessionStorage.JWT,
            "Content-Type": "application/json"
        })
    }).then(res => res.json())
}