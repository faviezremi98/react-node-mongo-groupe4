export const emailActivate = (id) => {
    return fetch('http://localhost:3000/mail/activate', {
        method: "POST",
        body: JSON.stringify({id}),
        headers: new Headers({
            "Content-Type": "application/json"
        })
    }).then(resp => resp.json());
}